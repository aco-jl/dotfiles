# Install all nerd fonts (paru or yay)
paru -S nerd-fonts-meta 

# Install NEOVIM and prerequisites
sudo pacman -S neovim 
sudo pacman -S python-pynvim

sudo pacman -S zsh zsh-completions
chsh -s /bin/zsh
# To do logout - login

#Oh my zsh
sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"

git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

#Open file
nvim ~/.zshrc
#Edit and Add next lines
plugins=(git
zsh-autosuggestions
zsh-syntax-highlighting
)
#Reload
source ~/.zshrc

sudo pacman -S zathura-pdf-mupdf
