--[[

	INIT.LUA BY ATANASIO 'GROCTEL' RUBIO ---- git@taxorubio.com

	Dependencies (as named in Arch official repos and AUR):
	> neovim-nightly-git : Latest version of Neovim. Required by many plugins.
	> ngrok : Set up public server for instant.nvim.
	> Language servers: Check lua/plugin.lua for LSPs and linters.

--]]


local impatient_ok, impatient = pcall(require, "impatient")
if impatient_ok then impatient.enable_profile() end

if vim.opt.compatible == true then vim.opt.compatible = false end

require "user"
require "plugins"
