require "user.options"
--require "user.autocmd"
require "user.filetype"
require "user.mappings"


-- Colour overrides

vim.cmd([[
	highlight! link CursorLineNr MatchParen
	highlight! link WinSeparator LineNr
]])
