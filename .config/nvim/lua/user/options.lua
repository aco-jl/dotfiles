local options = {	
	number = true, -- Number column to the left (used with relativenumber)
	relativenumber = true, -- Show numbers relative to cursor position (see number)
	numberwidth = 1, -- When you enable the number line it have space at the left.
                    -- I want the less space as possible.
	wrap = false, -- Do not wrap text that reaches the window's width
	linebreak = true, -- Respect WORDS when wrap-breaking lines (see wrap)


	hlsearch = true, -- Highlight all search matches
	incsearch = true, -- Highlight search matches while writing (with hlsearch)
	ignorecase = true, -- searches are case insensitive...
	smartcase = true, -- ... unless they contain at least one capital letter

	autoindent = true, -- Start new lines correctly indented
	smartindent = true, -- Ident new lines in a smart way (see autoindent)

	expandtab = false, -- Do not expand tabs to spaces (see softtabstop)
	smarttab = true, -- Treat spaces as tabs in increments of shiftwidth
	showtabline = 2, -- Show the tabline even when just one tab is open
	softtabstop = 0, -- Do not insert spaces when pressing tab (see shiftwidth)
	tabstop = 4, -- Number of columns to move when pressing <TAB> (see noexpandtab)
	shiftwidth = 0, -- Force indent spaces to equal to tabstop (see tabstop)


	splitbelow = true, -- Open splits below the current window
	splitright = true, -- Open splits right of the current window
	showcmd = true, -- Show the keys pressed in normal mode until action is run
	termguicolors = true, -- Enable 24-bit RGB color in the TUI
	cursorline = true, -- Highlight the line where the cursor is (see cursorlineopt)
	cursorlineopt = "number", -- Highlight the cursor line number (see cursorline)



	completeopt = {"menuone", "noselect"}, -- Completion engine options
	fillchars = vim.opt.fillchars + "diff:╱", -- Interface styling (see listchars)
	fixeol = true, -- Restore EOL at EOF if missing when writing
	foldmethod = "marker", -- Only allow foldings with triple brackets
	--guicursor = {a = "block"}, -- Force cursor to be a block at all times
	hidden = true, -- Hide inactive buffers instead of deleting them
	inccommand = "split", -- Incrementally show effects of commands, opens split
	laststatus = 3, -- Use a global statusline instead of one per window
	mouse = "nvi", -- Allow mouse everywhere except in command line mode
	nrformats = "unsigned", -- Treat all numbers as unsigned with <C-A> and <C-X>
	scrolloff = 5, -- Leave 5 lines above and below cursor
	signcolumn = "yes", -- Always show the sign column beside the number (see number)
	timeoutlen = 500, -- Milliseconds to wait before completing a mapped sequence
	updatetime = 300, -- Milliseconds to wait before writing to swap file
	wildignorecase = true, -- Ignore case in filenames browsed by wildmenu
}


for key, val in pairs(options) do
  vim.opt[key] = val
end


vim.g.mapleader=" " -- Set leader key to space to call which-key
vim.g.tex_flavor = "latex" -- Treat all .tex files as LaTeX instead of TeX
vim.opt.colorcolumn:append("81") -- Draw column at line character limit
vim.opt.shortmess:append("c")


-- TODO: Replace vim commands
vim.cmd([[
	function! CustomFold()
		return printf('   %-7d%s', v:foldend - v:foldstart + 1, getline(v:foldstart))
	endfunction

	set fillchars+=fold:\ | set foldtext=CustomFold() " Minimalistic folding
	set listchars=tab:\🭱\ ,trail:· list"Alternate tab: »>
]])
