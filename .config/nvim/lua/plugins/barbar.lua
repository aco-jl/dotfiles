vim.g.bufferline = {
	animation       = true,
	closable        = false,
	icons           = false,
	maximum_padding = 1,
	no_name_title   = "[New buffer]",
}
