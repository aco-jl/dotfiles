vim.g.nvim_tree_icons = {
	git = {
		unstaged =     " ",
		staged =       "🞥 ",
		unmerged =     " ",
		renamed =      " ",
		untracked =    " ",
		deleted =      " ",
	}
}


require("nvim-tree").setup({
	hijack_cursor = true,
	open_on_tab = true,
	sort_by = "extension",
	view = {
		width = 40,
	},
	renderer = {
		icons = {
			git_placement = "signcolumn",
		},
	},
})
