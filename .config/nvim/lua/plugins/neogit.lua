require("neogit").setup({
	integrations = {
		diffview = true,
	},
})


require("diffview").setup({
	file_panel = {
		position = "bottom",
	},
	key_bindings = {
		disable_defaults = false,
		file_panel = {
			['q'] = "<cmd>DiffviewClose<cr>",
		},
		option_panel = {
			['q'] = "<cmd>DiffviewClose<cr>",
		},
		view = {
			['q'] = "<cmd>DiffviewClose<cr>",
		},
	},
})
