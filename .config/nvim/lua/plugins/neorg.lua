local neorg_root = "~/.local/norg"


require("neorg").setup({
	load = {
		["core.defaults"] = {},
		["core.norg.concealer"] = {},
		["core.norg.dirman"] = {config = {
			workspaces = {
				root = neorg_root,

				home    = neorg_root.."/Home",
				journal = neorg_root.."/Journal",
				ugr     = neorg_root.."/UGR",
			}
		}},
		["core.norg.journal"] = {config = {
			workspace = journal,
		}},
		["core.gtd.base"] = {config = {
			workspace = "root"
		}},
		-- ["core.integrations.telescope"] = {},
	}
})


local parser_configs = require('nvim-treesitter.parsers').get_parser_configs()

parser_configs.norg_meta = {
	install_info = {
		url = "https://github.com/nvim-neorg/tree-sitter-norg-meta",
		files = { "src/parser.c" },
		branch = "main"
	},
}

parser_configs.norg_table = {
	install_info = {
		url = "https://github.com/nvim-neorg/tree-sitter-norg-table",
		files = { "src/parser.c" },
		branch = "main"
	},
}
