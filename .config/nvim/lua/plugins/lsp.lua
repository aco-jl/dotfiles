local lsp = require("lspconfig")


lsp.bashls.setup({})
lsp.intelephense.setup({})
lsp.pyright.setup({})
lsp.rust_analyzer.setup({})
lsp.texlab.setup({})
lsp.tsserver.setup({})
lsp.vimls.setup({})


lsp.cssls.setup({
	cmd = {"vscode-css-languageserver", "--stdio"}
})


lsp.html.setup({
	cmd = {"vscode-html-languageserver", "--stdio"}
})


lsp.sumneko_lua.setup({settings = { Lua = {
	diagnostics = {
		globals = {'vim'},
	},
}}})


require("clangd_extensions").setup({
	extensions = {
		ast = {
			role_icons = {
				declaration = '⯆',
				expression = '│',
				specifier = '',
				statement = '⯈',
				type = 'λ',
				['template argument'] = '',
			},
		},
		inlay_hints = {only_current_line = true},
		symbol_info = {border = "solid"},
	}
})


require("lint").linters_by_ft = {
	javascript = {"eslint"},
	sh         = {"shellcheck"},
	typescript = {"eslint"},
}


vim.cmd([[
	sign define DiagnosticSignError text=✘ texthl=DiagnosticSignError
	sign define DiagnosticSignWarn  text=⏺ texthl=DiagnosticSignWarn
	sign define DiagnosticSignInfo  text= texthl=DiagnosticSignInfo
	sign define DiagnosticSignHint  text= texthl=DiagnosticSignHint
]])
