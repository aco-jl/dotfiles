local fn = vim.fn


-- Automatically install packer
local install_path = fn.stdpath("data").."/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
	PACKER_BOOTSTRAP = fn.system({
		"git", "clone", "--depth", "1",
		"https://github.com/wbthomason/packer.nvim", install_path,
	})
	vim.cmd([[packadd packer.nvim]])
end


-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then return end


-- Have packer use a popup window
packer.init({
	display = {
		open_fn = function ()
			return require("packer.util").float({border = "rounded"})
		end,
	},
})


return packer.startup(function (use)
	-- Packer can manage itself as an optional plugin
	use "wbthomason/packer.nvim"


	-- Speed up loading Lua modules in Neovim to improve startup time.
	use "lewis6991/impatient.nvim"


	-- Icons
	use "kyazdani42/nvim-web-devicons"


	-- Theme
	use {"folke/tokyonight.nvim",
		config = function ()
			vim.g.tokyonight_style = "night"
			vim.cmd([[colorscheme tokyonight]])
		end,
	}


	-- Color highlighter, (example #558817)
	use {"norcalli/nvim-colorizer.lua", config = function () require "plugins.colorizer" end}


	-- A file explorer tree for neovim written in lua
	use {"kyazdani42/nvim-tree.lua", opt = true,
		cmd = "NvimTreeToggle",
		config = function () require "plugins.nvim-tree" end
	}


	-- LSP
	-- This plugin allows for declaratively configuring, launching, and initializing language servers you have installed on your system.
	use {
		--"williamboman/nvim-lsp-installer",
		"neovim/nvim-lspconfig",
		requires = {
			"p00f/clangd_extensions.nvim",
			"mfussenegger/nvim-lint",
		},
		config = function () require "plugins.lsp" end,
	}


	-- Autocomplete
	-- Treesitter
	-- Syntax
	-- Status Line and Bufferline
	-- Telescope
	-- Git
	-- Registers & Clipboard
	-- Move & Search & Replace
	-- 

	--All the lua functions I don't want to write twice.
	use "nvim-lua/plenary.nvim" -- Para Telescope
	use {"echasnovski/mini.nvim", config = function () require("mini.comment").setup() end}
	use {"nvim-treesitter/nvim-treesitter",
		requires = "nvim-treesitter/nvim-treesitter-textobjects",
		run = ":TSUpdate",
		config = function() require "plugins.treesitter" end
	}


	-- Completion and language server utilities
	-- use {"kosayoda/nvim-lightbulb", config = function () require "plugins.lightbulb" end}
	-- A completion engine plugin for neovim written in Lua.
	use {"hrsh7th/nvim-cmp",
		requires = {
			"hrsh7th/cmp-buffer",
			"hrsh7th/cmp-cmdline",
			"hrsh7th/cmp-latex-symbols",
			"hrsh7th/cmp-path",
			"onsails/lspkind.nvim",
			{"hrsh7th/cmp-nvim-lsp", requires = "neovim/nvim-lspconfig"},
			{"quangnguyen30192/cmp-nvim-ultisnips", requires = "sirver/UltiSnips"},
		},
		config = function () require "plugins.cmp" end
	}
	-- A pretty list for showing diagnostics, references, telescope results, quickfix and location lists to help you solve all the trouble your code is causing.
	use {"folke/trouble.nvim", opt = true,
		requires = "kyazdani42/nvim-web-devicons",
		cmd = "TroubleToggle",
	}
	
	-- Git
	use "tpope/vim-fugitive"
	use {"akinsho/git-conflict.nvim", config = function () require "plugins.git-conflict" end}
	use {"idanarye/vim-merginal", opt = true, cmd = {"Merginal"}}
	use {"lewis6991/gitsigns.nvim", config = function () require "plugins.gitsigns" end}
	--
	use {"TimUntersberger/neogit", opt = true, --
		cmd = {"DiffviewOpen", "Neogit"},
		requires = "sindrets/diffview.nvim",
		config = function () require "plugins.neogit" end,
	}


	-- Motions and editions
	use "chaoren/vim-wordmotion"
	use "matze/vim-move"
	use {"AndrewRadev/tagalong.vim", requires = "tpope/vim-repeat"}
	use {"inkarkat/vim-ReplaceWithRegister", requires = "tpope/vim-repeat"}
	use {"nmac427/guess-indent.nvim", config = function () require("guess-indent").setup({}) end}
	use {"tpope/vim-surround", requires = "tpope/vim-repeat"}
	use {"wellle/targets.vim"}
	use {"junegunn/vim-easy-align", --
		config = function ()
			vim.api.nvim_set_keymap("n", "ga", "<Plug>(EasyAlign)", {})
			vim.api.nvim_set_keymap("x", "ga", "<Plug>(EasyAlign)", {})
		end,
	}
	--
	use {"mattn/emmet-vim", --
		config = function ()
			vim.api.nvim_set_keymap("i", "<C-y><C-y>", "<Plug>(emmet-expand-abbr)", {})
		end,
	}
	--
	use {"sirver/UltiSnips", --
		config = function ()
			vim.g.UltiSnipsExpandTrigger       = "<Tab>"
			vim.g.UltiSnipsJumpForwardTrigger  = "<C-B>"
			vim.g.UltiSnipsJumpBackwardTrigger = "<C-Z>"
			vim.g.UltiSnipsEditSplit           = "vertical"
		end,
	}


	-- User interface
	use "voldikss/vim-floaterm"
	use {"folke/which-key.nvim", config = function () require "plugins.which-key" end}
	use {"glepnir/dashboard-nvim", config = function () require "plugins.dashboard" end}


	--A pretty list for showing diagnostics, references, telescope results, quickfix and location lists to help you solve all the trouble your code is causing.
	--use {"folke/trouble.nvim", opt = true,
	--	requires = "kyazdani42/nvim-web-devicons",
	--	cmd = "TroubleToggle",
	--}
	--A minimal, stylish and customizable statusline / winbar for Neovim written in Lua
	--use {"feline-nvim/feline.nvim",
	use { 'feline-nvim/feline.nvim', branch = '0.5-compat',
		requires = "SmiteshP/nvim-gps",
		--require('feline').winbar.setup()
	---require('feline').setup(),
		config = function () require "plugins.feline" end,
	}
	--use {'nvim-lualine/lualine.nvim',
	--	requires = { 'kyazdani42/nvim-web-devicons', opt = true },
	--	config = function () require "plugins.lualine" end,
	--}
	
	-- In-editor sprinkling
	-- highlight and search for todo comments like:
	-- TODO, 
	-- HACK, 
	-- BUG 
	-- in your code base.
	use {"folke/todo-comments.nvim", requires = "nvim-lua/plenary.nvim", config = function () require("todo-comments").setup({}) end}
	
	
	-- Distraction-free writing in Vim.
	use {"junegunn/goyo.vim", opt = true,
		cmd = "Goyo",
		requires = "junegunn/limelight.vim",
	}



	-- The plug-in visualizes undo history and makes it easier to browse and switch between different undo branches.
	use {"mbbill/undotree", opt = true,
		cmd = "UndotreeToggle",
		config = function ()
			vim.g.undotree_SetFocusWhenToggle = 1
			vim.g.undotree_ShortIndicators    = 1
			vim.g.undotree_TreeNodeShape      = "◯"
			vim.g.undotree_WindowLayout       = 2
		end,
	}

--This is a Debug Adapter Protocol client that allows you to:
--Launch an application to debug
--Attach to running applications and debug them
--Set breakpoints and step through code
--Inspect the state of the application
--	use {"mfussenegger/nvim-dap", opt = true, 
--		requires = "rcarriga/nvim-dap-ui"
--	}


	-- highly extendable fuzzy finder over lists
	use {"nvim-telescope/telescope.nvim", opt = true,
	cmd = "Telescope",
		requires = {
			"kyazdani42/nvim-web-devicons",
			-- "nvim-telescope/telescope-ui-select.nvim"
		},
		config = function () require "plugins.telescope" end,
	}

	-- User-friendly framework for building your dream tabline in a few lines of code.
	use {"rafcamlet/tabline-framework.nvim",
		requires = "kyazdani42/nvim-web-devicons",
		config = function () require "plugins.tabline" end,
	}

	-- This plugin provides a handy pop-up menu for code actions.
	use {"weilbith/nvim-code-action-menu", opt = true,
		cmd = "CodeActionMenu",
	}



	-- Miscellanea
	--use {"Groctel/jobsplit.vim", opt = true, cmd = {"Jobclose", "Jobrepeat", "Jobsplit"}}
	--use {"andweeb/presence.nvim",
	--	config = function ()
	--		vim.g.presence_auto_update       = 1
	--		vim.g.presence_neovim_image_text = "gitlab.com/groctel"
	--	end,
	--}

	--use {"nvim-neorg/neorg",
	--	requires = {
	--		"nvim-treesitter/nvim-treesitter",
	--		"nvim-neorg/neorg-telescope"
	--	},
	--	config = function () require "plugins.neorg" end,
	--}


	
	if PACKER_BOOTSTRAP then require("packer").sync() end
end)
