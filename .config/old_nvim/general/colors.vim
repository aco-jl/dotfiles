"" The most basic configurations
syntax on               " Turn on syntax highlighting.
set termguicolors       " Activa true colors en la terminal
set background=dark     " Fondo del tema: ligth o dark
" adjust python highlight
let g:python_highlight_all = 1

