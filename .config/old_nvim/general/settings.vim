"" Set the map leader
let mapleader = " "

set encoding=utf-8
set spelllang=en,es     " Corregir palabras usando diccionarios en inglés y español
set mouse=a             " this enable the mouse compatibility
set clipboard=unnamedplus " To set the main clipboard to vim. It needs xclip o xcel. Other value is unnamed
set noerrorbells        " This disable the error bells because is very annoying

"" To set the tabs
set sw=4                " this set the tabs are 4 spaces
set expandtab           " Insert spaces instead of <Tab>s
set smartindent         " this saves work to you to indent your code

"" Show numbres
set number              " Show line numbers
set rnu                 " Show relative numbers
set numberwidth=1       " When you enable the number line it have space at the left.
                        " I want the less space as possible. 
set nowrap              " I don't like the wrapped lines :v

" set the cursorline and a column
set cursorline          " highlight the current cursor line
set colorcolumn=120     " display a ruler at a specific line
highlight ColoColumn ctermbg=0 guibg=lightgrey

"" Searching
set hlsearch            " highlight matches
set incsearch           " incremental searching
set ignorecase          " searches are case insensitive...
set smartcase           " ... unless they contain at least one capital letter

"this sets the directions of the splits
set splitbelow          " make the new window appear below the current window.
set splitright          " make the new window appear on the right.

"I HATE the f*cking swap files and I don't want the backup files
set noswapfile
set nobackup
set undodir=~/.config/nvim/.undodir/
set undofile

"" Neovide config
set guifont=Fira\ Code:h10
let g:neovide_transparency=0.7
let g:neovide_no_idle=v:true
"let g:neovide_fullscreen=v:true


