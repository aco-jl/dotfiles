local function repeat_str (str, times)
	return times > 0 and str..repeat_str(str, times-1) or ""
end


local header = {
	[[ ███╗   ██╗███████╗ ██████╗ ██╗   ██╗██╗███╗   ███╗]],
	[[ ████╗  ██║██╔════╝██╔═══██╗██║   ██║██║████╗ ████║]],
	[[ ██╔██╗ ██║█████╗  ██║   ██║██║   ██║██║██╔████╔██║]],
	[[ ██║╚██╗██║██╔══╝  ██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║]],
	[[ ██║ ╚████║███████╗╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║]],
	[[ ╚═╝  ╚═══╝╚══════╝ ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝]],
	[[                                                   ]]
}

local splash = vim.fn.system("shuf -n 1 "..vim.fn.stdpath("config").."/splashes"):sub(1,-2).."!"
local padding = (#header[#header] - #splash) / 2 - 2
splash = repeat_str(" ", padding).."[ "..splash.." ]"

-- Add the splash string followed by two empty lines
table.insert(header, splash)


vim.g.dashboard_custom_header  = header
vim.g.dashboard_custom_section = {
	row0 = {
		description = {" New file                                 :enew"},
		command = "enew",
	},
	row1 = {
		description = {" Find file                                SPC f"},
		command = "Telescope find_files hidden=true no_ignore=true",
	},
	row2 = {
		description = {" Browse dotfiles                        SPC v d"},
		command = "Telescope find_files cwd=~/.config/nvim/ search_dirs=Ultisnips,lua,viml,init.vim",
	},
	row3 = {
		description = {" Update plugins                     :PackerSync"},
		command = "PackerSync",
	},
	row4 = {
		description = {" Open floating terminal                 SPC t t"},
		command = "FloatermToggle",
	},
	row5 = {
		description = {" Close neovim                              :qa!"},
		command = "qa!",
	},
}


local augroup = vim.api.nvim_create_augroup("AU_dashboard", {clear = true})
vim.api.nvim_create_autocmd("FileType", {
	pattern = "dashboard",
	group = augroup,
	callback = function ()
		vim.api.nvim_buf_set_keymap(0, "n", "q", "<cmd>quit!<cr>", {noremap = true, silent = true})
	end,
})
