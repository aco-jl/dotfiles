local fn = vim.fn


-- Automatically install packer
local install_path = fn.stdpath("data").."/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
	PACKER_BOOTSTRAP = fn.system({
		"git", "clone", "--depth", "1",
		"https://github.com/wbthomason/packer.nvim", install_path,
	})
	vim.cmd([[packadd packer.nvim]])
end


-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then return end


-- Have packer use a popup window
packer.init({
	display = {
		open_fn = function ()
			return require("packer.util").float({border = "rounded"})
		end,
	},
})


return packer.startup(function (use)
	use "wbthomason/packer.nvim"
	-- My plugins here
	use "kyazdani42/nvim-web-devicons"
	use "lewis6991/impatient.nvim" --Speed up loading Lua modules in Neovim to improve startup time.
	use "nvim-lua/plenary.nvim"
	use {"echasnovski/mini.nvim", config = function () require("mini.comment").setup() end}
	use {"nvim-treesitter/nvim-treesitter",
		requires = "nvim-treesitter/nvim-treesitter-textobjects",
		run = ":TSUpdate",
		config = function() require "plugins.treesitter" end
	}


	-- User interface
	use "voldikss/vim-floaterm" --Use (neo)vim terminal in the floating/popup window.
	use {"folke/which-key.nvim", config = function () require "plugins.which-key" end} --displays a popup with possible key bindings of the command you started typing
	use {"glepnir/dashboard-nvim", config = function () require "plugins.dashboard" end}
	use {"feline-nvim/feline.nvim",
		requires = "SmiteshP/nvim-gps",
		config = function () require "plugins.feline" end,
	}

	use {"folke/tokyonight.nvim",
		config = function ()
			vim.g.tokyonight_style = "night"
			vim.cmd([[colorscheme tokyonight]])
		end,
	}


	-- Automatically set up your configuration after cloning packer.nvim
	-- Put this at the end after all plugins
	if PACKER_BOOTSTRAP then require("packer").sync() end
end)

