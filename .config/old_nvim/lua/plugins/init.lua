local fn = vim.fn


-- Automatically install packer
local install_path = fn.stdpath("data").."/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
	PACKER_BOOTSTRAP = fn.system({
		"git", "clone", "--depth", "1",
		"https://github.com/wbthomason/packer.nvim", install_path,
	})
	vim.cmd([[packadd packer.nvim]])
end


-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then return end


-- Have packer use a popup window
packer.init({
	display = {
		open_fn = function ()
			return require("packer.util").float({border = "rounded"})
		end,
	},
})


return packer.startup(function (use)
	use "wbthomason/packer.nvim"
	use "kyazdani42/nvim-web-devicons"
	use "lewis6991/impatient.nvim"
	use "nvim-lua/plenary.nvim"
	use {"echasnovski/mini.nvim", config = function () require("mini.comment").setup() end}
	use {"nvim-treesitter/nvim-treesitter", -- {{{1
		requires = "nvim-treesitter/nvim-treesitter-textobjects",
		run = ":TSUpdate",
		config = function() require "plugins.treesitter" end
	}
	-- }}}1

	-- Completion and language server utilities {{{1
	use {"kosayoda/nvim-lightbulb", config = function () require "plugins.lightbulb" end}
	use {"hrsh7th/nvim-cmp", -- {{{2
		requires = {
			"hrsh7th/cmp-buffer",
			"hrsh7th/cmp-cmdline",
			"hrsh7th/cmp-latex-symbols",
			"hrsh7th/cmp-path",
			"onsails/lspkind.nvim",
			{"hrsh7th/cmp-nvim-lsp", requires = "neovim/nvim-lspconfig"},
			{"quangnguyen30192/cmp-nvim-ultisnips", requires = "sirver/UltiSnips"},
		},
		config = function () require "plugins.cmp" end
	}
	-- }}}2
	use {"folke/trouble.nvim", opt = true, -- {{{2
		requires = "kyazdani42/nvim-web-devicons",
		cmd = "TroubleToggle",
	}
	-- }}}2
	use {"neovim/nvim-lspconfig", -- {{{2
		requires = {
			"p00f/clangd_extensions.nvim",
			"mfussenegger/nvim-lint",
		},
		config = function () require "plugins.lsp" end,
	}
	-- }}}2
	-- }}}1

	-- Git {{{1
	use "tpope/vim-fugitive"
	use {"akinsho/git-conflict.nvim", config = function () require "plugins.git-conflict" end}
	use {"idanarye/vim-merginal", opt = true, cmd = {"Merginal"}}
	use {"lewis6991/gitsigns.nvim", config = function () require "plugins.gitsigns" end}
	-- }}}2
	use {"TimUntersberger/neogit", opt = true, -- {{{2
		cmd = {"DiffviewOpen", "Neogit"},
		requires = "sindrets/diffview.nvim",
		config = function () require "plugins.neogit" end,
	}
	-- }}}2
	-- }}}1

	-- In-editor sprinkling {{{1
	use {"folke/todo-comments.nvim", config = function () require("todo-comments").setup({}) end}
	use {"norcalli/nvim-colorizer.lua", config = function () require "plugins.colorizer" end}
	-- }}}1

	-- Miscellanea {{{1
	use {"Groctel/jobsplit.vim", opt = true, cmd = {"Jobclose", "Jobrepeat", "Jobsplit"}}
	use {"andweeb/presence.nvim", -- {{{2
		config = function ()
			vim.g.presence_auto_update       = 1
			vim.g.presence_neovim_image_text = "gitlab.com/groctel"
		end,
	}
	-- }}}2
	use {"nvim-neorg/neorg", -- {{{2
		requires = {
			"nvim-treesitter/nvim-treesitter",
			"nvim-neorg/neorg-telescope"
		},
		config = function () require "plugins.neorg" end,
	}
	-- }}}2
	-- }}}1

	-- Motions and editions {{{1
	use "chaoren/vim-wordmotion"
	use "matze/vim-move"
	use {"AndrewRadev/tagalong.vim", requires = "tpope/vim-repeat"}
	use {"inkarkat/vim-ReplaceWithRegister", requires = "tpope/vim-repeat"}
	use {"nmac427/guess-indent.nvim", config = function () require("guess-indent").setup({}) end}
	use {"tpope/vim-surround", requires = "tpope/vim-repeat"}
	use {"wellle/targets.vim"}
	use {"junegunn/vim-easy-align", -- {{{2
		config = function ()
			vim.api.nvim_set_keymap("n", "ga", "<Plug>(EasyAlign)", {})
			vim.api.nvim_set_keymap("x", "ga", "<Plug>(EasyAlign)", {})
		end,
	}
	-- }}}2
	use {"mattn/emmet-vim", -- {{{2
		config = function ()
			vim.api.nvim_set_keymap("i", "<C-y><C-y>", "<Plug>(emmet-expand-abbr)", {})
		end,
	}
	-- }}}2
	use {"sirver/UltiSnips", -- {{{2
		config = function ()
			vim.g.UltiSnipsExpandTrigger       = "<Tab>"
			vim.g.UltiSnipsJumpForwardTrigger  = "<C-B>"
			vim.g.UltiSnipsJumpBackwardTrigger = "<C-Z>"
			vim.g.UltiSnipsEditSplit           = "vertical"
		end,
	}
	-- }}}2
	-- }}}1

	-- User interface {{{1
	use "voldikss/vim-floaterm"
	use {"folke/which-key.nvim", config = function () require "plugins.which-key" end}
	use {"glepnir/dashboard-nvim", config = function () require "plugins.dashboard" end}
	use {"feline-nvim/feline.nvim", -- {{{2
		requires = "SmiteshP/nvim-gps",
		config = function () require "plugins.feline" end,
	}
	-- }}}2
	use {"folke/tokyonight.nvim", -- {{{2
		config = function ()
			vim.g.tokyonight_style = "night"
			vim.cmd([[colorscheme tokyonight]])
		end,
	}
	-- }}}2
	use {"junegunn/goyo.vim", opt = true, -- {{{2
		cmd = "Goyo",
		requires = "junegunn/limelight.vim",
	}
	-- }}}2
	use {"kyazdani42/nvim-tree.lua", opt = true, -- {{{2
		cmd = "NvimTreeToggle",
		config = function () require "plugins.nvim-tree" end
	}
	-- }}}2
	use {"mbbill/undotree", opt = true, -- {{{2
		cmd = "UndotreeToggle",
		config = function ()
			vim.g.undotree_SetFocusWhenToggle = 1
			vim.g.undotree_ShortIndicators    = 1
			vim.g.undotree_TreeNodeShape      = "◯"
			vim.g.undotree_WindowLayout       = 2
		end,
	}
	-- }}}2
	use {"mfussenegger/nvim-dap", opt = true, -- {{{2
		requires = "rcarriga/nvim-dap-ui"
	}
	-- }}}2
	use {"nvim-telescope/telescope.nvim", opt = true, -- {{{2
		cmd = "Telescope",
		requires = {
			"kyazdani42/nvim-web-devicons",
			-- "nvim-telescope/telescope-ui-select.nvim"
		},
		config = function () require "plugins.telescope" end,
	}
	-- }}}2
	use {"rafcamlet/tabline-framework.nvim", -- {{{2
		requires = "kyazdani42/nvim-web-devicons",
		config = function () require "plugins.tabline" end,
	}
	-- }}}2
	use {"weilbith/nvim-code-action-menu", opt = true, -- {{{2
		cmd = "CodeActionMenu",
	}
	-- }}}2
	-- }}}1

	-- use "puremourning/vimspector"
	--use "/home/groctel/Documents/Git/Groctel/pddl.vim"

	--------------------------------------------------------------

	-- use "moll/vim-bbye"
	-- use "akinsho/toggleterm.nvim"
	-- use "ahmedkhalf/project.nvim"
	-- use "antoinemadec/FixCursorHold.nvim" -- This is needed to fix lsp doc highlight

	-- -- snippets
	-- use "L3MON4D3/LuaSnip" --snippet engine
	-- use "rafamadriz/friendly-snippets" -- a bunch of snippets to use
	if PACKER_BOOTSTRAP then require("packer").sync() end
end)
