local colors = require("tokyonight.colors").setup()
local gps    = require("nvim-gps")

gps.setup({
	separator = "  ",
})


local WINBAR = {}

WINBAR[1] = {
	provider = function() return gps.get_location() end,
	enabled = function() return gps.is_available() end,
	hl = {
		bg = colors.bg_dark,
		fg = colors.fg,
	},
}

return WINBAR
