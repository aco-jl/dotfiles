local impatient_ok, impatient = pcall(require, "impatient")
if impatient_ok then impatient.enable_profile() end

if vim.opt.compatible == true then vim.opt.compatible = false end

require "plugins"
require "user"

