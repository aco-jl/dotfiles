#!/bin/bash

# Screens
#hdmi=`xrandr | grep ' connected' | grep 'HDMI' | awk '{print $1}'`

#if [ "$hdmi" = "HDMI-1-0" ]; then
#    xrandr --output HDMI-1-0 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output eDP-1 --mode 1920x1080 --pos 1920x0 --rotate normal &
#else
#    xrandr --output eDP-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal &
#fi

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

#starting utility applications at boot time
lxsession &
run nm-applet &
run pamac-tray &
numlockx on &
blueman-applet &
#flameshot &
#picom --config $HOME/.config/picom/picom.conf &
picom --config .config/picom/picom-blur.conf --experimental-backends &
#/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
dunst &
#feh --randomize --bg-fill /usr/share/wallpapers/garuda-wallpapers/*
#starting user applications at boot time
run volumeicon &
# systray battery icon
cbatticon -u 5 &
#run discord &
#nitrogen --random --set-zoom-fill &
#run caffeine -a &
#run vivaldi-stable &
#run firefox &
#run thunar &
#run dropbox &
#run insync start &
#run spotify &
#run atom &
#run telegram-desktop &

#feh --bg-fill $HOME/.config/qtile/wallpapers/bg1.png --bg-fill $HOME/.config/qtile/wallpapers/bg2.png 
feh --bg-fill $HOME/.config/qtile/wallpapers/bg1.png 
#feh --bg-fill $HOME/.config/qtile/wallpapers/bg2.png 
feh --bg-fill $HOME/.config/qtile/wallpapers/bg1.png --bg-fill $HOME/.config/qtile/wallpapers/bg2.png 
