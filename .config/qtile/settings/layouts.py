from libqtile import layout
from libqtile.config import Match
from .theme import colors

# Layouts and layout rules

def init_layout_theme():
    return {"margin": 5,
            "border_width": 1,
            "border_focus": "#ff00ff",
            "border_normal": "#f4c2c2"
            }


layout_theme = init_layout_theme()


layouts = [
    layout.MonadTall(margin=5, border_width=1,
                     border_focus="#ff00ff", border_normal="#f4c2c2"),
    layout.MonadWide(margin=5, border_width=1,
                     border_focus="#ff00ff", border_normal="#f4c2c2"),
    layout.Matrix(**layout_theme),
    layout.Bsp(**layout_theme),
    layout.Floating(**layout_theme),
    layout.RatioTile(**layout_theme),
    layout.Max(**layout_theme),
    layout.Columns(**layout_theme),
    layout.Stack(**layout_theme),
    layout.Tile(**layout_theme),
    layout.TreeTab(
        sections=['FIRST', 'SECOND'],
        bg_color='#141414',
        active_bg='#0000ff',
        inactive_bg='#1e90ff',
        padding_y=5,
        section_top=10,
        panel_width=280),
    layout.VerticalTile(**layout_theme),
    layout.Zoomy(**layout_theme)
]

'''
floating_layout = layout.Floating(
    float_rules=[
        *layout.Floating.default_float_rules,
        Match(wm_class='confirmreset'),
        Match(wm_class='makebranch'),
        Match(wm_class='maketag'),
        Match(wm_class='ssh-askpass'),
        Match(title='branchdialog'),
        Match(title='pinentry'),
    ],
    border_focus=colors["color4"][0]
)
'''