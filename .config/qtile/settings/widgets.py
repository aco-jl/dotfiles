import os 
import socket
from libqtile import widget, qtile
from .theme import colors
#from qtile_extras import widget
#from qtile_extras.widget.decorations import BorderDecoration

DoomOne = [
    ["#00000001", "#00000001"], # bg
    ["#bbc2cfff", "#bbc2cfff"], # fg
    ["#1c1f24ff", "#1c1f24ff"], # color01
    ["#ff6c6bff", "#ff6c6bff"], # color02
    ["#98be65ff", "#98be65ff"], # color03
    ["#da8548ff", "#da8548ff"], # color04
    ["#51afefff", "#51afefff"], # color05
    ["#c678ddff", "#c678ddff"], # color06
    ["#46d9ffff", "#46d9ffff"]  # color15
    ]

myTerm = "alacritty"  # My terminal of choice


#def base(fg='text', bg='dark'):
#    return {'foreground': colors[fg], 'background': colors[bg]}

def base(fg='text', bg='dark'):
    return {'foreground': DoomOne[1], 'background': DoomOne[0]}

def icon(background=DoomOne[0], foreground=DoomOne[7], fontsize=17, text="?"):
    return widget.TextBox(
        #**base(fg, bg),
        background=background,
        foreground=foreground,
        font='UbuntuMono Nerd Font',
        fontsize=fontsize,
        text=text,
        padding=0,
    )


# WIDGETS FOR THE BAR


def init_widgets_defaults():
    return dict(
        font="Noto Sans Bold",
        fontsize=14,
        padding=3,
        background=DoomOne[0],
    )


widget_defaults = init_widgets_defaults()


def init_widgets_list():
    prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())
    widgets_list = [
                     
        widget.Image(
            filename="~/.config/qtile/icons/garuda-purple.png",
            iconsize=9,
            margin_y=2,
            margin_x=10,
            mouse_callbacks={'Button1': lambda: qtile.cmd_spawn('jgmenu_run')}
        ),
        
        widget.GroupBox(
            #**base(fg='light'),
            font='UbuntuMono Nerd Font Bold',

            fontsize=15,
            margin_y=2,
            margin_x=1,
            padding_y=2,
            padding_x=3,
            borderwidth=1,

            active=colors['active'],
            inactive=colors['inactive'],
            rounded=True,
            highlight_method='block',
            urgent_alert_method='block',
            urgent_border=colors['urgent'],
            this_current_screen_border=colors['focus'],
            this_screen_border=colors['grey'],
            other_current_screen_border=colors['dark'],
            other_screen_border=colors['dark'],
            disable_drag=True
        ),
        
        icon(fontsize=19, text='󰤃 '),
        
        widget.CurrentLayoutIcon(
            custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons")],
            padding=-2,
            scale=0.7,
            #**base(bg='color4'),
            background=DoomOne[0],
            foreground=DoomOne[1],
        ),

        widget.CurrentLayout(
            #**base(bg='color4'),
            background=DoomOne[0],
            foreground=DoomOne[1],
            #padding=3,
        ),
        
        icon(fontsize=19, text='󰤃'),
        
        #widget.Spacer(length = 400),
        
        widget.TaskList(
            background=DoomOne[0],
            foreground=DoomOne[1],
            highlight_method='border',  # or block
            icon_size=20,
            max_title_width=200,
            rounded=True,
            padding_x=3,
            padding_y=1.5,
            margin_y=0,
            margin_x=5,
            font='UbuntuMono Nerd Font',
            fontsize=16,
            border=colors['focus'],
            margin=2,
            txt_floating='🗗',
            txt_minimized='>_ ',
            borderwidth=1,
            #unfocused_border = 'border'
        ),

        icon(fontsize=17, text=' 󱍢 '),
        
        widget.NetGraph(
            #**base(bg='color3'),
            background=DoomOne[0],
            foreground=DoomOne[1],
            fill_color= '1667EB.3',
            graph_color= '18BAEB',
            border_width= 0,
            type= 'line',
            line_width= 1,
        ),
        
        #icon(bg="color4", fontsize=17, text='󰃰 '),

        #widget.Net(
        #    #**base(bg='color3'),
        #    background=DoomOne[0],
        #    foreground=DoomOne[1],
        #    # Here enter your network name
        #    interface=["wlp61s0"],
        #    prefix='M',
        #    format='{down}  {up}',
        #),

        widget.Spacer(length = 15),
        
        icon(fontsize=17, text=' '),

        widget.CPU(
            background=DoomOne[0],
            foreground=DoomOne[1],
            format = '{load_percent}%',
            update_interval=1,
            mouse_callbacks={
                'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e htop')},
        ),

        widget.Spacer(length = 15),
        
        icon(fontsize=17, text='󰌢 '),

        widget.Memory(
            background=DoomOne[0],
            foreground=DoomOne[1],
            #format='{MemUsed: .1f}G /{MemTotal: .1f}G',
            format = '{MemUsed: .1f}{mm}',
            #fmt = '󰌢  {} used',
            fmt = '{} used',
            update_interval=1,
            measure_mem='G',
            mouse_callbacks={
                'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e htop')},
        ),

        # widget.ThermalSensor(
        #     **base(bg='color1',fg='color2'),
        #     #foreground = colors[4],
        #     #background = colors[0],
        #     threshold = 90,
        #     fmt = 'Temp: {}',
        #     padding = 5,
        # ),
        
        widget.Spacer(length = 15),
        
        icon(fontsize=17, text='󰌌 '),

        widget.KeyboardLayout(
            #**base(bg='color3'),
            background=DoomOne[0],
            foreground=DoomOne[1],
            font='UbuntuMono Nerd Font Bold',
            fontsize=16,
            configured_keyboards=['es', 'us'],
            display_map={'es': 'ES', 'us': 'US'},
            #display_map={'es': '󰥻 ES', 'us': '󰌌 US'},
        ),

        widget.Spacer(length = 15),

        # Icon: nf-mdi-calendar_clock
        icon(fontsize=17, text='󰃰 '),

        widget.Clock(
            #**base(bg='color4'),
            background=DoomOne[0],
            foreground=DoomOne[1],
            #format="%d-%m-%Y  %H:%M",
            format = "%a, %b %d - %H:%M",
            mouse_callbacks={
                'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e calcurse')},
            #decorations=[BorderDecoration(colour = DoomOne[1],border_width = [0, 0, 2, 0],)],
        ),

        widget.Spacer(length = 15),

        widget.Systray(
            background=DoomOne[0],
            foreground=DoomOne[1],
            icon_size=20,
            padding=4
        ),
    ]
    return widgets_list


widgets_list = init_widgets_list()
